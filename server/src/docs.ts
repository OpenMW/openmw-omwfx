import { TokenType } from "./types";

export const Docs: { [key: string]: string } = {

[TokenType.Shared]:
`
GLSL code block.

Any code declared here will be available to any shader pass.
`
,

"author":
`The name of the author or authors associated with the file.

Displayed in the in-game HUD.
`
,

"version":
`A version string associated with the file.

Displayed in the in-game HUD.
`
,

"passes":
`Comma seperated list of passes.

At least one pass name must be in this list.

*Passes can be repeated.*
`
,

"default":
`The default value of the uniform.

Users can use the in-game HUD to reset the value back to default.
`
,

"min":
`The minimum value of the uniform.
`
,

"max":
`The maximum value of the uniform.
`
,

"size":
`The number of elements in the uniform.

Setting the size parameter will automatically enable the 'static' field.
Uniform arrays can only be controlled via Lua, and not from the in-game HUD.
---
*Boolean arrays are not supported*
`
,

"step":
`
The amount the value is incremented with mouse wheel and arrow clicks in the in-game HUD.

It is recommended to always set this value so the in-game sliders are easy to use.
`
,

"static":
`
Indicates if the uniform will need to be modified from outside the in-game HUD.
Currently, this needs to be set if you need to control the uniform from a Lua script.

If true, the uniform will not be controllable from the in-game HUD.
`
,

"description":
`
The description that will display in the in-game HUD.

This field is optional, but it is recommended to always give a short, useful description.
`
,

"header":
`
If set, a group header with a visual delimter will be placed before this uniform in the in-game HUD.

Uniforms are shown in the order in which they are defined in the HUD. If you have multiple uniforms which
should be grouped together, you can set this parameter on the first uniform in the group.
`
,

"display_name":
`
The name of the uniform that is shown in the in-game HUD.

Within the shader files, the uniform is referred to by its original name.
`
,

"min_filter":
`
The min filter set on the texture.

Defaults to \`LINEAR_MIPMAP_LINEAR\`.
`
,
"mag_filter":
`
The mag filter set on the texture.

Defaults to \`LINEAR\`.
`
,
"wrap_s":
`
The wrap mode set on the texture.

Defaults to \`CLAMP_TO_EDGE\`.
`
,
"wrap_t":
`
The wrap mode set on the texture.

Defaults to \`CLAMP_TO_EDGE\`.

`
,
"wrap_r":
`
The wrap mode set on the texture.

Defaults to \`CLAMP_TO_EDGE\`.

*Only applicable to 3D textures*
`
,
"source":
`
Required: The path of the texture relative to the VFS.

This file can be located in any folder, but it is recommended to create a folder in the \`shaders\` subdirectory to avoid naming clashes with game textures.
`
,
"source_type":
`
Sets the external source data for the texture.

Auto-computed by default.

*If you are unsure how to use this correctly, leave this option out.*
`
,
"source_format":
`
Sets the external source image format for the texture.

Auto-computed by default.

*If you are unsure how to use this correctly, leave this option out.*
`
,
"internal_format":
`
Sets the internal texture format for the texture.

Auto-computed by default.

*If you are unsure how to use this correctly, leave this option out.*
`
,
"compression":
`
Sets the internal texture format mode for the texture.

Auto-computed by default.

*If you are unsure how to use this correctly, leave this option out.*
`,


"mipmaps":
`
Whether to generate mipmaps every frame for the texture associated with render target framebuffer.

Defaults to false.
`,

"width":
`
The width of the render target framebuffer in pixels.
`,

"height":
`
The height of the render target framebuffer in pixels.
`,

"width_ratio":
`
The width of the render target framebuffer as a ratio of primary render target resolution.

A value of 0.5 will set the width to half of the primary render target resolution.

*This handles runtime changes to primary render resolution.*
`,


"height_ratio":
`
The height of the render target framebuffer as a ratio of primary render target resolution.

A value of 0.5 will set the height to half of the primary render target resolution.

*This handles runtime changes to primary render resolution.*
`,

};