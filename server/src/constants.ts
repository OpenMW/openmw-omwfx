export namespace Constants {
    export enum TechniqueFlag {
        Flag_Disable_Interiors,
        Flag_Disable_Exteriors,
        Flag_Disable_Underwater,
        Flag_Disable_Abovewater,
        Flag_Disable_SunGlare,
        Flag_Hidden,
    }

    export enum SourceFormat {
        GL_RED,
        GL_RG,
        GL_RGB,
        GL_BGR,
        GL_RGBA,
        GL_BGRA,
    }

    export enum SourceType {
        GL_BYTE,
        GL_UNSIGNED_BYTE,
        GL_SHORT,
        GL_UNSIGNED_SHORT,
        GL_INT,
        GL_UNSIGNED_INT,
        GL_UNSIGNED_INT_24_8,
        GL_FLOAT,
        GL_DOUBLE,
    }

    export enum InternalFormat {
        GL_RED,
        GL_R16F,
        GL_R32F,
        GL_RG,
        GL_RG16F,
        GL_RG32F,
        GL_RGB,
        GL_RGB16F,
        GL_RGB32F,
        GL_RGBA,
        GL_RGBA16F,
        GL_RGBA32F,
        GL_DEPTH_COMPONENT16,
        GL_DEPTH_COMPONENT24,
        GL_DEPTH_COMPONENT32,
        GL_DEPTH_COMPONENT32F,
    }

    export enum BlendFunc {
        DST_ALPHA,
        DST_COLOR,
        ONE,
        ONE_MINUS_DST_ALPHA,
        ONE_MINUS_DST_COLOR,
        ONE_MINUS_SRC_ALPHA,
        ONE_MINUS_SRC_COLOR,
        SRC_ALPHA,
        SRC_ALPHA_SATURATE,
        SRC_COLOR,
        CONSTANT_COLOR,
        ONE_MINUS_CONSTANT_COLOR,
        CONSTANT_ALPHA,
        ONE_MINUS_CONSTANT_ALPHA,
        ZERO,
    }

    export enum Compression {
        USE_USER_DEFINED_FORMAT,
        USE_ARB_COMPRESSION,
        USE_S3TC_DXT1_COMPRESSION,
        USE_S3TC_DXT3_COMPRESSION,
        USE_S3TC_DXT5_COMPRESSION,
        USE_PVRTC_2BPP_COMPRESSION,
        USE_PVRTC_4BPP_COMPRESSION,
        USE_ETC_COMPRESSION,
        USE_ETC2_COMPRESSION,
        USE_RGTC1_COMPRESSION,
        USE_RGTC2_COMPRESSION,
        USE_S3TC_DXT1c_COMPRESSION,
        USE_S3TC_DXT1a_COMPRESSION,
    }

    export enum WrapMode {
        CLAMP,
        CLAMP_TO_EDGE,
        CLAMP_TO_BORDER,
        REPEAT,
        MIRROR,
    }

    export enum FilterMode {
        LINEAR,
        LINEAR_MIPMAP_LINEAR,
        LINEAR_MIPMAP_NEAREST,
        NEAREST,
        NEAREST_MIPMAP_LINEAR,
        NEAREST_MIPMAP_NEAREST,
    }

    export enum BlendEquation {
        RGBA_MIN,
        RGBA_MAX,
        ALPHA_MIN,
        ALPHA_MAX,
        LOGIC_OP,
        FUNC_ADD,
        FUNC_SUBTRACT,
        FUNC_REVERSE_SUBTRACT,
    }

    export const TechniqueFlagMap = {
        disable_interiors: TechniqueFlag.Flag_Disable_Interiors,
        disable_exteriors: TechniqueFlag.Flag_Disable_Exteriors,
        disable_underwater: TechniqueFlag.Flag_Disable_Underwater,
        disable_abovewater: TechniqueFlag.Flag_Disable_Abovewater,
        disable_sunglare: TechniqueFlag.Flag_Disable_SunGlare,
        hidden: TechniqueFlag.Flag_Hidden,
    };

    export const SourceFormatMap = {
        red: SourceFormat.GL_RED,
        rg: SourceFormat.GL_RG,
        rgb: SourceFormat.GL_RGB,
        bgr: SourceFormat.GL_BGR,
        rgba: SourceFormat.GL_RGBA,
        bgra: SourceFormat.GL_BGRA,
    };

    export const SourceTypeMap = {
        byte: SourceType.GL_BYTE,
        unsigned_byte: SourceType.GL_UNSIGNED_BYTE,
        short: SourceType.GL_SHORT,
        unsigned_short: SourceType.GL_UNSIGNED_SHORT,
        int: SourceType.GL_INT,
        unsigned_int: SourceType.GL_UNSIGNED_INT,
        unsigned_int_24_8: SourceType.GL_UNSIGNED_INT_24_8,
        float: SourceType.GL_FLOAT,
        double: SourceType.GL_DOUBLE,
    };

    export const InternalFormatMap = {
        red: InternalFormat.GL_RED,
        r16f: InternalFormat.GL_R16F,
        r32f: InternalFormat.GL_R32F,
        rg: InternalFormat.GL_RG,
        rg16f: InternalFormat.GL_RG16F,
        rg32f: InternalFormat.GL_RG32F,
        rgb: InternalFormat.GL_RGB,
        rgb16f: InternalFormat.GL_RGB16F,
        rgb32f: InternalFormat.GL_RGB32F,
        rgba: InternalFormat.GL_RGBA,
        rgba16f: InternalFormat.GL_RGBA16F,
        rgba32f: InternalFormat.GL_RGBA32F,
        depth_component16: InternalFormat.GL_DEPTH_COMPONENT16,
        depth_component24: InternalFormat.GL_DEPTH_COMPONENT24,
        depth_component32: InternalFormat.GL_DEPTH_COMPONENT32,
        depth_component32f: InternalFormat.GL_DEPTH_COMPONENT32F,
    };

    export const CompressionMap = {
        auto: Compression.USE_USER_DEFINED_FORMAT,
        arb: Compression.USE_ARB_COMPRESSION,
        s3tc_dxt1: Compression.USE_S3TC_DXT1_COMPRESSION,
        s3tc_dxt3: Compression.USE_S3TC_DXT3_COMPRESSION,
        s3tc_dxt5: Compression.USE_S3TC_DXT5_COMPRESSION,
        pvrtc_2bpp: Compression.USE_PVRTC_2BPP_COMPRESSION,
        pvrtc_4bpp: Compression.USE_PVRTC_4BPP_COMPRESSION,
        etc: Compression.USE_ETC_COMPRESSION,
        etc2: Compression.USE_ETC2_COMPRESSION,
        rgtc1: Compression.USE_RGTC1_COMPRESSION,
        rgtc2: Compression.USE_RGTC2_COMPRESSION,
        s3tc_dxt1c: Compression.USE_S3TC_DXT1c_COMPRESSION,
        s3tc_dxt1a: Compression.USE_S3TC_DXT1a_COMPRESSION,
    };

    export const WrapModeMap = {
        clamp: WrapMode.CLAMP,
        clamp_to_edge: WrapMode.CLAMP_TO_EDGE,
        clamp_to_border: WrapMode.CLAMP_TO_BORDER,
        repeat: WrapMode.REPEAT,
        mirror: WrapMode.MIRROR,
    };

    export const FilterModeMap = {
        linear: FilterMode.LINEAR,
        linear_mipmap_linear: FilterMode.LINEAR_MIPMAP_LINEAR,
        linear_mipmap_nearest: FilterMode.LINEAR_MIPMAP_NEAREST,
        nearest: FilterMode.NEAREST,
        nearest_mipmap_linear: FilterMode.NEAREST_MIPMAP_LINEAR,
        nearest_mipmap_nearest: FilterMode.NEAREST_MIPMAP_NEAREST,
    };

    export const BlendFuncMap = {
        dst_alpha: BlendFunc.DST_ALPHA,
        dst_color: BlendFunc.DST_COLOR,
        one: BlendFunc.ONE,
        one_minus_dst_alpha: BlendFunc.ONE_MINUS_DST_ALPHA,
        one_minus_dst_color: BlendFunc.ONE_MINUS_DST_COLOR,
        one_minus_src_alpha: BlendFunc.ONE_MINUS_SRC_ALPHA,
        one_minus_src_color: BlendFunc.ONE_MINUS_SRC_COLOR,
        src_alpha: BlendFunc.SRC_ALPHA,
        src_alpha_saturate: BlendFunc.SRC_ALPHA_SATURATE,
        src_color: BlendFunc.SRC_COLOR,
        constant_color: BlendFunc.CONSTANT_COLOR,
        one_minus_constant_color: BlendFunc.ONE_MINUS_CONSTANT_COLOR,
        constant_alpha: BlendFunc.CONSTANT_ALPHA,
        one_minus_constant_alpha: BlendFunc.ONE_MINUS_CONSTANT_ALPHA,
        zero: BlendFunc.ZERO,
    };

    export const BlendEquationMap = {
        rgba_min: BlendEquation.RGBA_MIN,
        rgba_max: BlendEquation.RGBA_MAX,
        alpha_min: BlendEquation.ALPHA_MIN,
        alpha_max: BlendEquation.ALPHA_MAX,
        logic_op: BlendEquation.LOGIC_OP,
        add: BlendEquation.FUNC_ADD,
        subtract: BlendEquation.FUNC_SUBTRACT,
        reverse_subtract: BlendEquation.FUNC_REVERSE_SUBTRACT,
    };
}
