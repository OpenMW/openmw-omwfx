import { Range, Position } from "vscode-languageserver/node";

export function rangeContains(range: Range, position: Position): boolean {
    if (range.start.line > position.line || range.end.line < position.line) {
        return false;
    }
    if (range.end.line === position.line && range.end.character <= position.character) {
        return false;
    }
    return true;
}