import { DocumentLink, CompletionItem, Range, Position, DocumentSymbol, SymbolKind, DiagnosticSeverity, Diagnostic, CompletionItemKind } from 'vscode-languageserver';
import { rangeContains } from './util';
import { Constants } from './constants';
import { VoidFunction, Token, TokenType, Block, Pass } from './types'

export interface ExtendedDocumentSymbol extends DocumentSymbol {
    token?: Token,
    docRange?: Range
}

export class Parser {
    private _buffer = '';
    private _head = 0;
    private _tail = 0;
    private _line = 0;
    private _col = 0;
    private _lastToken: Token = { type: TokenType.Eof, line: -1, col: -1 };
    private _lookAhead: Token | null = null;
    private _lastJumpBlock: Block = { line: 0 };

    private _token: Token = { type: TokenType.Eof, line: -1, col: -1};
    private _blockName = '';
    private _passMap: Map<string, Pass> = new Map<string, Pass>();
    private _uniforms: Map<string, any> = new Map<string, any>();
    private _renderTargets: Map<string, any> = new Map<string, any>();

    public symbols: Array<ExtendedDocumentSymbol> = [];
    public diagnostics: Diagnostic[] = [];

    private _uri = '';

    private _rootSymbol: ExtendedDocumentSymbol | undefined = undefined;
    private _keySymbol: ExtendedDocumentSymbol | undefined = undefined;
    private _links: Array<DocumentLink> = [];

    private _layout: {[key: string] : { [key: string] : any }} = {
        [TokenType.Technique]: {
            spec: {
                "passes": { exec: this.parseLiteralList },
                "version": { exec: this.parseString },
                "description": { exec: this.parseString },
                "author": { exec: this.parseString },
                "glsl_version": { exec: this.parseNumber },
                "flags": { exec: this.parseLiteralList },
                "hdr": { exec: this.parseBool },
                "pass_normals": { exec: this.parseBool },
                "pass_lights": { exec: this.parseBool },
                "glsl_profile": { exec: this.parseString },
                "glsl_extensions": { exec: this.parseLiteralList },
                "dynamic": { exec: this.parseBool },
            }
        },
        [TokenType.Render_Target]: {
            spec: {
                "min_filter": { exec: this.parseConstant, args: Constants.FilterModeMap },
                "mag_filter": { exec: this.parseConstant, args: Constants.FilterModeMap },
                "wrap_s": { exec: this.parseConstant, args: Constants.WrapModeMap },
                "wrap_t": { exec: this.parseConstant, args: Constants.WrapModeMap },
                "width_ratio": { exec: this.parseNumber},
                "height_ratio": { exec: this.parseNumber },
                "width": { exec: this.parseNumber },
                "height": { exec: this.parseNumber },
                "internal_format": { exec: this.parseConstant, args: Constants.InternalFormatMap },
                "source_type": { exec: this.parseConstant, args: Constants.SourceTypeMap },
                "source_format": { exec: this.parseConstant, args: Constants.SourceFormatMap },
                "mipmaps": { exec: this.parseBool },
            }
        },
        [TokenType.Sampler_1D]: {
            spec: {
                "min_filter": { exec: this.parseConstant, args: Constants.FilterModeMap },
                "mag_filter": { exec: this.parseConstant, args: Constants.FilterModeMap },
                "wrap_s": { exec: this.parseConstant, args: Constants.WrapModeMap },
                "wrap_t": { exec: this.parseConstant, args: Constants.WrapModeMap },
                "source": { exec: this.parseString },
                "source_type": { exec: this.parseConstant, args: Constants.SourceTypeMap },
                "source_format": { exec: this.parseConstant, args: Constants.SourceFormatMap },
                "internal_format": { exec: this.parseConstant, args: Constants.InternalFormatMap },
                "compression": { exec: this.parseConstant, args: Constants.CompressionMap },
            }
        },
        [TokenType.Sampler_2D]: {
            spec: {
                "min_filter": { exec: this.parseConstant, args: Constants.FilterModeMap },
                "mag_filter": { exec: this.parseConstant, args: Constants.FilterModeMap },
                "wrap_s": { exec: this.parseConstant, args: Constants.WrapModeMap },
                "wrap_t": { exec: this.parseConstant, args: Constants.WrapModeMap },
                "source": { exec: this.parseString },
                "source_type": { exec: this.parseConstant, args: Constants.SourceTypeMap },
                "source_format": { exec: this.parseConstant, args: Constants.SourceFormatMap },
                "internal_format": { exec: this.parseConstant, args: Constants.InternalFormatMap },
                "compression": { exec: this.parseConstant, args: Constants.CompressionMap },
            }
        },
        [TokenType.Sampler_3D]: {
            spec: {
                "min_filter": { exec: this.parseConstant, args: Constants.FilterModeMap },
                "mag_filter": { exec: this.parseConstant, args: Constants.FilterModeMap },
                "wrap_s": { exec: this.parseConstant, args: Constants.WrapModeMap },
                "wrap_t": { exec: this.parseConstant, args: Constants.WrapModeMap },
                "wrap_r": { exec: this.parseConstant, args: Constants.WrapModeMap },
                "source": { exec: this.parseString },
                "source_type": { exec: this.parseConstant, args: Constants.SourceTypeMap },
                "source_format": { exec: this.parseConstant, args: Constants.SourceFormatMap },
                "internal_format": { exec: this.parseConstant, args: Constants.InternalFormatMap },
                "compression": { exec: this.parseConstant, args: Constants.CompressionMap },
            }
        },
        [TokenType.Uniform_Bool]: {
            spec: {
                "default": { exec: this.parseBool },
                "min": { exec: this.parseBool },
                "max": { exec: this.parseBool },
                "size": { exec: this.parseNumber },
                "step": { exec: this.parseNumber },
                "static": { exec: this.parseBool },
                "description": { exec: this.parseString },
                "header": { exec: this.parseString },
                "display_name": { exec: this.parseString },
            }
        },
        [TokenType.Uniform_Float]: {
            spec: {
                "default": { exec: this.parseNumber },
                "min": { exec: this.parseNumber },
                "max": { exec: this.parseNumber },
                "size": { exec: this.parseNumber },
                "step": { exec: this.parseNumber },
                "static": { exec: this.parseBool },
                "description": { exec: this.parseString },
                "header": { exec: this.parseString },
                "display_name": { exec: this.parseString },
            }
        },
        [TokenType.Uniform_Int]: {
            spec: {
                "default": { exec: this.parseNumber },
                "min": { exec: this.parseNumber },
                "max": { exec: this.parseNumber },
                "size": { exec: this.parseNumber },
                "step": { exec: this.parseNumber },
                "static": { exec: this.parseBool },
                "description": { exec: this.parseString },
                "header": { exec: this.parseString },
                "display_name": { exec: this.parseString },
            }
        },
        [TokenType.Uniform_Vec2]: {
            spec: {
                "default": { exec: this.parseVector, args: 2 },
                "min": { exec: this.parseVector, args: 2 },
                "max": { exec: this.parseVector, args: 2 },
                "size": { exec: this.parseNumber },
                "step": { exec: this.parseNumber },
                "static": { exec: this.parseBool },
                "description": { exec: this.parseString },
                "header": { exec: this.parseString },
                "display_name": { exec: this.parseString },
            }
        },
        [TokenType.Uniform_Vec3]: {
            spec: {
                "default": { exec: this.parseVector, args: 3 },
                "min": { exec: this.parseVector, args: 3 },
                "max": { exec: this.parseVector, args: 3 },
                "size": { exec: this.parseNumber },
                "step": { exec: this.parseNumber },
                "static": { exec: this.parseBool },
                "description": { exec: this.parseString },
                "header": { exec: this.parseString },
                "display_name": { exec: this.parseString },
            }
        },
        [TokenType.Uniform_Vec4]: {
            spec: {
                "default": { exec: this.parseVector, args: 4 },
                "min": { exec: this.parseVector, args: 4 },
                "max": { exec: this.parseVector, args: 4 },
                "size": { exec: this.parseNumber },
                "step": { exec: this.parseNumber },
                "static": { exec: this.parseBool },
                "description": { exec: this.parseString },
                "header": { exec: this.parseString },
                "display_name": { exec: this.parseString },
            }
        }
    };

    constructor(uri: string) {
        this._uri = uri;
    }

    public getLinks() : DocumentLink[] {
        return this._links;
    }

    public getRootSymbolAtPosition(position: Position): ExtendedDocumentSymbol | undefined {
        for(const symbol of this.symbols) {
            if (rangeContains(symbol.range, position)) {
                return symbol;
            }
        }
    }

    public getLeafSymbolAtPosition(position: Position): ExtendedDocumentSymbol | undefined {
        for(const symbol of this.symbols) {
            if (rangeContains(symbol.range, position)) {
                if (!symbol.children) {
                    return symbol;
                }
                for (const child of symbol.children) {
                    if (rangeContains(child.range, position)) {
                        return child;
                    }
                }
                return symbol;
            }
        }
    }

    public parse(buffer: string) {
        this._buffer = buffer;
        this._head = 0;
        this._tail = buffer.length;
        this._line = 0;
        this._col = 0;
        this._lastToken = { type: TokenType.Eof, line: -1, col: -1 };
        this._lookAhead = null;

        this._token = { type: TokenType.Eof, line: -1, col: -1 };
        this._blockName = '';
        this._passMap = new Map<string, Pass>();
        this._uniforms = new Map<string, any>();
        this._renderTargets = new Map<string, any>();

        this.symbols = [];
        this.diagnostics = [];

        this._rootSymbol = undefined;
        this._keySymbol = undefined;
        this._links = [];

        for (
            this._token = this.next();
            this._token.type != TokenType.Eof;
            this._token = this.next()
        ) {
            if (this._token.type === TokenType.Shared) {
                this.parseBlock_Shared();
            } else if (this._token.type === TokenType.Technique) {
                this.parseBlock_Technique();
            } else if (this._token.type === TokenType.Render_Target) {
                this.parseBlock_Render_Target();
            } else if (this._token.type === TokenType.Vertex) {
                this.parseBlock_Shader();
            } else if (this._token.type === TokenType.Fragment) {
                this.parseBlock_Shader();
            } else if (this._token.type === TokenType.Compute) {
                this.parseBlock_Shader();
            } else if (this._token.type === TokenType.Sampler_1D) {
                this.parseBlock_Uniform(this._token.type);
            } else if (this._token.type === TokenType.Sampler_2D) {
                this.parseBlock_Uniform(this._token.type);
            } else if (this._token.type === TokenType.Sampler_3D) {
                this.parseBlock_Uniform(this._token.type);
            } else if (this._token.type === TokenType.Uniform_Bool) {
                this.parseBlock_Uniform(this._token.type);
            } else if (this._token.type === TokenType.Uniform_Float) {
                this.parseBlock_Uniform(this._token.type);
            } else if (this._token.type === TokenType.Uniform_Int) {
                this.parseBlock_Uniform(this._token.type);
            } else if (this._token.type === TokenType.Uniform_Vec2) {
                this.parseBlock_Uniform(this._token.type);
            } else if (this._token.type === TokenType.Uniform_Vec3) {
                this.parseBlock_Uniform(this._token.type);
            } else if (this._token.type === TokenType.Uniform_Vec4) {
                this.parseBlock_Uniform(this._token.type);
            } else {
                this.error(`invalid top level block '${this._token.value}'`);
            }
        }
    }

    public getCompletion(position: Position): CompletionItem[] {
        const items: CompletionItem[] = [];
        const symbol = this.getLeafSymbolAtPosition(position);

        if (!symbol) {
            return items;
        }

        /*
        if (symbol.name === 'passes') {
            for (const key of this._passMap.keys()) {
                items.push({
                    label: `${key}`,
                    kind: CompletionItemKind.Value,
                    data: null,
                    commitCharacters: [',', ';'],
                });
            }
        }*/

        return items;
    }

    private expect(type: TokenType, msg?: string, ignoreDiagnostic?: boolean) {
        this._token = this.next();
        if (type !== this._token.type) {
            if (msg) {
                this.error(`${msg}. Expected ${TokenType[type]} but got ${TokenType[this._token.type]}`, ignoreDiagnostic);
            } else {
                this.error(`Expected ${TokenType[type]} but got ${TokenType[this._token.type]}`, ignoreDiagnostic);
            }
            return false;
        }

        return true;
    }

    private parseConstant(map: any) : any {
        this.expect(TokenType.Literal);

        for (const [key, value] of Object.entries(map)) {
            if (this._token.value == key) {
                return value;
            }
        }

        this.error(`unrecognized constant ${this._token.value}`);
    }

    private parseBool() : boolean {
        this._token = this.next();

        if (this._token.type === TokenType.True) {
            return true;
        }

        if (this._token.type === TokenType.False) {
            return false;
        }

        this.error("expected 'true' or 'false' as boolean value");
        return false;
    }

    private parseNumber() {
        this._token = this.next();

        if (this._token.type === TokenType.Integer || this._token.type === TokenType.Float) {
            return this._token.value;
        }

        this.error("expected number value");
    }

    private parseString() {
        this.expect(TokenType.String);

        return this._token.value;
    }

    private parseLiteral() {
        this.expect(TokenType.Literal);

        return this._token.value;
    }

    private parseLiteralList() {
        const sep = TokenType.Comma;
        const data: Array<string> = [];

        while (this.peek().type !== TokenType.Eof) {
            this.expect(TokenType.Literal);

            data.push(this._token.value);

            if (this.peek().type !== sep) {
                break;
            }

            this.next();
        }

        return data;
    }

    private parseVector(size: number) : any {
        const v: number[] = [];

        const typeMap: Map<number, TokenType> = new Map<number, TokenType>([
            [4, TokenType.Vec4],
            [3, TokenType.Vec3],
            [2, TokenType.Vec2],
        ]);

        this.expect(typeMap.get(size)!);
        this.expect(TokenType.Open_Parenthesis);

        for (let i = 0; i < size; ++i) {
            v.push(this.parseNumber());

            if (i < (size - 1)) {
                this.expect(TokenType.Comma);
            }
        }

        this.expect(TokenType.Close_Parenthesis, "check definition of the vector");

        return v;
    }

    private parseBlockHeader(): void {
        this.expect(TokenType.Open_Parenthesis);

        if (this.peek().type === TokenType.Close_Parenthesis) {
            this.next();
            return;
        }

        const pass: Pass = { renderTargets: [null, null, null] };

        while (this.peek().type !== TokenType.Eof) {
            this.expect(TokenType.Literal, 'invalid key in block header');

            const key = this._token.value;

            this.expect(TokenType.Equal);

            if (key === 'target') {
                pass.target = this.parseLiteral();
            } else if (key === 'rt1') {
                pass.renderTargets[0] = this.parseLiteral();
            } else if (key === 'rt2') {
                pass.renderTargets[1] = this.parseLiteral();
            } else if (key === 'rt3') {
                pass.renderTargets[2] = this.parseLiteral();
            } else if (key === 'blend') {
                this.expect(TokenType.Open_Parenthesis);
                const blendEq = this.parseConstant(Constants.BlendEquationMap);
                this.expect(TokenType.Comma);
                const blendSrc = this.parseConstant(Constants.BlendFuncMap);
                this.expect(TokenType.Comma);
                const blendDest = this.parseConstant(Constants.BlendFuncMap);
                this.expect(TokenType.Close_Parenthesis);

                pass.blendSource = blendSrc;
                pass.blendDest = blendDest;
                if (blendEq !== Constants.BlendEquation.FUNC_ADD) {
                    pass.blendEq = blendEq;
                }
            } else if (key === 'clear') {
                pass.clear = this.parseBool();
            } else if (key === 'clear_color') {
                pass.clearColor = this.parseVector(4);
            } else {
                this.error(`unrecognized key '${key}' in block header`);
            }

            this._token = this.next();

            if (this._token.type === TokenType.Comma) {
                if (this.peek().type === TokenType.Close_Parenthesis) {
                    this.error(
                        `leading comma in '${this._blockName}' is not allowed`
                    );
                } else {
                    continue;
                }
            }

            if (this._token.type === TokenType.Close_Parenthesis) return;
        }

        this._passMap.set(this._blockName, pass);

        this.error('malformed block header');
    }

    private parseBlock_Shared() {
        this._blockName = TokenType[this._token.type];

        this._rootSymbol = {
            token: this._token,
            docRange: Range.create(this._token.line, this._token.col, this._token.line, this._token.col + 'shared'.length),
            name: this._blockName,
            detail: "Code",
            kind: SymbolKind.Class,
            range: Range.create(this._token.line, this._token.col, 0, 0),
            selectionRange: Range.create(this._token.line, this._token.col, 0, 0),
        };
        this.symbols.push(this._rootSymbol);

        this.expect(TokenType.Open_bracket);
        this.jump();
        this.expect(TokenType.Close_bracket);

        this._rootSymbol.range.end = { line: this._line, character: this._col };
        this._rootSymbol.selectionRange.end = { line: this._line, character: this._col };
    }

    private parseBlock_Shader() {
        this._blockName = TokenType[this._token.type];

        const blockType = this._token.type;
        const blockName = this._blockName;

        this.expect(
            TokenType.Literal,
            'name is required for preceeding block decleration'
        );

        const nameRange = Range.create(this._token.line, this._token.col, this._token.line, this._token.col + this._token.value.length);

        this._blockName = this._token.value;

        this._rootSymbol = {
            name: this._blockName,
            detail: blockName,
            kind: SymbolKind.Class,
            range: Range.create(this._token.line, this._token.col, 0, 0),
            selectionRange: Range.create(this._token.line, this._token.col, 0, 0),
        };
        this.symbols.push(this._rootSymbol);

        if (this.peek().type === TokenType.Open_Parenthesis) {
            this.parseBlockHeader();
        }

        this.expect(TokenType.Open_bracket);

        if (!this.jump()) {
            this.error(`unterminated '${TokenType[blockType]}' block, expected closing brackets`);
        }

        let pass: any = this._passMap.get(this._blockName);

        if (!pass) {
            this._passMap.set(this._blockName, { renderTargets: [null, null, null] });
            pass = this._passMap.get(this._blockName);
        }

        if (blockType === TokenType.Vertex) {
            if (pass.vertex) {
                this.diagnostics.push(Diagnostic.create(nameRange, `Duplicate vertex pass '${this._blockName}'`, DiagnosticSeverity.Error));
            }
            pass.vertex = this.getLastJumpBlock();
        } else if (blockType === TokenType.Fragment) {
            if (pass.fragment) {
                this.diagnostics.push(Diagnostic.create(nameRange, `Duplicate fragment pass '${this._blockName}'`, DiagnosticSeverity.Error));
            }
            pass.fragment = this.getLastJumpBlock();
        } else if (blockType === TokenType.Compute) {
            if (pass.compute) {
                this.diagnostics.push(Diagnostic.create(nameRange, `Duplicate compute pass '${this._blockName}'`, DiagnosticSeverity.Error));
            }
            pass.compute = this.getLastJumpBlock();
        }

        pass.name = this._blockName;
        pass.type = blockType;

        this.expect(TokenType.Close_bracket);

        this._rootSymbol.range.end = { line: this._line, character: this._col };
        this._rootSymbol.selectionRange.end = { line: this._line, character: this._col };
    }

    private parseBlock_Technique() {
        this._blockName = TokenType[this._token.type];

        this._rootSymbol = {
            name: this._blockName,
            kind: SymbolKind.Class,
            range: Range.create(this._token.line, this._token.col, 0, 0),
            selectionRange: Range.create(this._token.line, this._token.col, 0, 0),
            children: [],
        };
        this.symbols.push(this._rootSymbol);

        this.expect(TokenType.Open_bracket);

        const config = this._layout[TokenType.Technique].spec;

        while (this.peek().type !== TokenType.Close_bracket && this.peek().type !== TokenType.Eof) {
            this.tryExpect(TokenType.Literal, 3);

            const key = this._token.value;
            this._keySymbol = {
                name: key,
                kind: SymbolKind.Field,
                range: Range.create(this._token.line, this._token.col, 0, 0),
                selectionRange: Range.create(this._token.line, this._token.col, 0, 0),
            };
            this._rootSymbol.children?.push(this._keySymbol);

            const item = config[key];

            if (!item) {
                this.error(`unexpected key '${key}'`);
            }

            this.expect(TokenType.Equal);

            if (item) {
                item.exec.call(this, item.args);
            }

            this.expect(TokenType.SemiColon);

            this._keySymbol.range.end = { line: this._line, character: this._col };
            this._keySymbol.selectionRange.end = { line: this._line, character: this._col };
        }

        this.expect(TokenType.Close_bracket);

        this._rootSymbol.range.end = { line: this._line, character: this._col };
        this._rootSymbol.selectionRange.end = { line: this._line, character: this._col };
    }

    private parseBlock_Render_Target() {
        this._blockName = TokenType[this._token.type];

        this._rootSymbol = {
            name: this._blockName,
            detail: "Render Target",
            kind: SymbolKind.Class,
            range: Range.create(this._token.line, this._token.col, 0, 0),
            selectionRange: Range.create(this._token.line, this._token.col, 0, 0),
            children: [],
        };
        this.symbols.push(this._rootSymbol);

        this.expect(
            TokenType.Literal,
            'name is required for preceeding block decleration'
        );

        this._blockName = this._token.value;

        this._rootSymbol.name = this._blockName;

        if (this._blockName in this._renderTargets) {
            this.error(`redeclaration of render target '${this._blockName}'`);
        }

        this.expect(TokenType.Open_bracket);

        const config = this._layout[TokenType.Render_Target].spec;

        while (this.peek().type !== TokenType.Close_bracket && this.peek().type !== TokenType.Eof) {
            this.tryExpect(TokenType.Literal, 3);

            const key = this._token.value;
            this._keySymbol = {
                name: key,
                kind: SymbolKind.Field,
                range: Range.create(this._token.line, this._token.col, 0, 0),
                selectionRange: Range.create(this._token.line, this._token.col, 0, 0),
            };
            this._rootSymbol.children?.push(this._keySymbol);

            const item = config[key];

            if (!item) {
                this.error(`unexpected key '${key}'`);
            }

            this.expect(TokenType.Equal);

            if (item) {
                item.exec.call(this, item.args);
            }

            this.expect(TokenType.SemiColon);

            this._keySymbol.range.end = { line: this._line, character: this._col };
            this._keySymbol.selectionRange.end = { line: this._line, character: this._col };
        }

        this._renderTargets.set(this._blockName, '__TODO__FILL');

        this.expect(TokenType.Close_bracket);

        this._rootSymbol.range.end = { line: this._line, character: this._col };
        this._rootSymbol.selectionRange.end = { line: this._line, character: this._col };
    }

    private parseBlock_Uniform(type : TokenType) {
        this._blockName = TokenType[this._token.type];

        const blockType = this._token.type;

        this._rootSymbol = {
            name: this._blockName,
            detail: `Uniform`,
            kind: SymbolKind.Class,
            range: Range.create(this._token.line, this._token.col, 0, 0),
            selectionRange: Range.create(this._token.line, this._token.col, 0, 0),
            children: [],
        };
        this.symbols.push(this._rootSymbol);

        this.expect(
            TokenType.Literal,
            'name is required for preceeding block decleration'
        );

        this._blockName = this._token.value;

        this._rootSymbol.name = this._blockName;

        if (this._blockName in this._uniforms) {
            this.error(`redeclaration of uniform '${this._blockName}'`);
        }

        this.expect(TokenType.Open_bracket);

        const layout = this._layout[blockType];
        if (!layout) {
            this.error(`unexpected block name '${layout}'`);
        }

        const config = layout.spec;

        while (this.peek().type !== TokenType.Close_bracket && this.peek().type !== TokenType.Eof) {
            this.tryExpect(TokenType.Literal, 3);

            const key = this._token.value;

            this._keySymbol = {
                name: key,
                kind: SymbolKind.Field,
                range: Range.create(this._token.line, this._token.col, 0, 0),
                selectionRange: Range.create(this._token.line, this._token.col, 0, 0),
            };
            this._rootSymbol.children?.push(this._keySymbol);

            const item = config[key];

            if (!item) {
                this.error(`unexpected key '${key}'`);
            }

            this.expect(TokenType.Equal);

            if (item) {
                item.exec.call(this, item.args);
            }

            this.expect(TokenType.SemiColon);

            this._keySymbol.range.end = { line: this._line, character: this._col };
            this._keySymbol.selectionRange.end = { line: this._line, character: this._col };
        }

        this._uniforms.set(this._blockName, '__TODO__FILL');

        this.expect(TokenType.Close_bracket);

        this._rootSymbol.range.end = { line: this._line, character: this._col };
        this._rootSymbol.selectionRange.end = { line: this._line, character: this._col };
    }

    private drop(): void {
        this._lookAhead = null;
    }

    private advance(): void {
        this._head++;
        this._col++;
    }

    private head(): string {
        return this._buffer[this._head];
    }

    private peekChar(c: string): boolean {
        if (
            this._head === this._tail ||
            this._head > this._buffer.length
        ) {
            return false;
        }

        return this._buffer[this._head + 1] === c;
    }

    private scanToken(): Token {
        while (true) {
            if (this._head === this._tail) {
                return { type: TokenType.Eof, line: -1, col: -1 };
            }

            let ch = this.head();

            if (ch === '\r' && this.peekChar('\n')) {
                this._head++;
                this._col++;

                ch = this.head();
            }

            if (ch === '\n') {
                this._line++;
                this._col = 0;
            }

            if (!(ch === ' ' || ch === '\t' || ch === '\n')) {
                break;
            }

            this.advance();
        }

        if (this.head() === '"') {
            return this.scanStringLiteral();
        }

        if (/[A-Z]/i.test(this.head())) {
            return this.scanLiteral();
        }

        if (
            /\d/.test(this.head()) ||
            this.head() === '.' ||
            this.head() === '-'
        ) {
            return this.scanNumber();
        }

        switch (this.head()) {
            case '=':
                this.advance();
                return { type: TokenType.Equal, line: this._line, col: this._col };
            case '{':
                this.advance();
                return { type: TokenType.Open_bracket, line: this._line, col: this._col };
            case '}':
                this.advance();
                return { type: TokenType.Close_bracket, line: this._line, col: this._col };
            case '(':
                this.advance();
                return { type: TokenType.Open_Parenthesis, line: this._line, col: this._col };
            case ')':
                this.advance();
                return { type: TokenType.Close_Parenthesis, line: this._line, col: this._col };
            case '"':
                this.advance();
                return { type: TokenType.Quote, line: this._line, col: this._col };
            case ':':
                this.advance();
                return { type: TokenType.Colon, line: this._line, col: this._col };
            case ';':
                this.advance();
                return { type: TokenType.SemiColon, line: this._line, col: this._col };
            case '|':
                this.advance();
                return { type: TokenType.VBar, line: this._line, col: this._col };
            case ',':
                this.advance();
                return { type: TokenType.Comma, line: this._line, col: this._col };
            default:
                this.error(`Unexpected token <${this.head()}>`);
        }

        this.error('critical error');
        return { type: TokenType.Eof, line: -1, col: -1 };
    }

    private scanLiteral(): Token {
        const start = this._head;

        this.advance();

        const lbegin = this._line;
        const cbegin = Math.max(0, this._col - 2);

        const isalnum = (str: string): boolean => {
            let code: number, i: number, len: number;

            for (i = 0, len = str.length; i < len; i++) {
                code = str.charCodeAt(i);
                if (
                    !(code > 47 && code < 58) && // numeric (0-9)
                    !(code > 64 && code < 91) && // upper alpha (A-Z)
                    !(code > 96 && code < 123)
                ) {
                    // lower alpha (a-z)
                    return false;
                }
            }
            return true;
        };

        while (
            this._head != this._tail &&
            (isalnum(this.head()) || this.head() === '_')
        ) {
            this.advance();
        }

        const value = this._buffer.substring(
            start,
            start + (this._head - start)
        );

        if (value === 'shared') return { type: TokenType.Shared, line: lbegin, col: cbegin };
        if (value === 'technique') return { type: TokenType.Technique, line: lbegin, col: cbegin };
        if (value === 'render_target') return { type: TokenType.Render_Target, line: lbegin, col: cbegin };
        if (value === 'vertex') return { type: TokenType.Vertex, line: lbegin, col: cbegin };
        if (value === 'fragment') return { type: TokenType.Fragment, line: lbegin, col: cbegin };
        if (value === 'compute') return { type: TokenType.Compute, line: lbegin, col: cbegin };
        if (value === 'sampler_1d') return { type: TokenType.Sampler_1D, line: lbegin, col: cbegin };
        if (value === 'sampler_2d') return { type: TokenType.Sampler_2D, line: lbegin, col: cbegin };
        if (value === 'sampler_3d') return { type: TokenType.Sampler_3D, line: lbegin, col: cbegin };
        if (value === 'uniform_bool') return { type: TokenType.Uniform_Bool, line: lbegin, col: cbegin };
        if (value === 'uniform_float') return { type: TokenType.Uniform_Float, line: lbegin, col: cbegin };
        if (value === 'uniform_int') return { type: TokenType.Uniform_Int, line: lbegin, col: cbegin };
        if (value === 'uniform_vec2') return { type: TokenType.Uniform_Vec2, line: lbegin, col: cbegin };
        if (value === 'uniform_vec3') return { type: TokenType.Uniform_Vec3, line: lbegin, col: cbegin };
        if (value === 'uniform_vec4') return { type: TokenType.Uniform_Vec4, line: lbegin, col: cbegin };
        if (value === 'true') return { type: TokenType.True, line: lbegin, col: cbegin };
        if (value === 'false') return { type: TokenType.False, line: lbegin, col: cbegin };
        if (value === 'vec2') return { type: TokenType.Vec2, line: lbegin, col: cbegin };
        if (value === 'vec3') return { type: TokenType.Vec3, line: lbegin, col: cbegin };
        if (value === 'vec4') return { type: TokenType.Vec4, line: lbegin, col: cbegin };

        return { type: TokenType.Literal, value: value, line: lbegin, col: cbegin };
    }

    private scanStringLiteral(): Token {
        this.advance(); // consume quote
        const start = this._head;

        const lbegin = this._line - 1;
        const cbegin = this._col - 1;

        let terminated = false;

        for (; this._head !== this._tail; this.advance()) {
            if (this.head() === '"') {
                terminated = true;
                this.advance();
                break;
            }
        }

        if (!terminated) this.error('unterminated string');

        return {
            type: TokenType.String,
            value: this._buffer.substring(
                start,
                start + (this._head - start - 1)
            ),
            line: lbegin,
            col: cbegin,
        };
    }

    private scanNumber(): Token {
        // TODO: regex with substring sucks! Just do it with manual lookaheads.
        const regexNumber = /[+-]?([0-9]*[.])?[0-9]*/;

        const lbegin = this._line;
        const cbegin = this._col;

        const match = this._buffer
            .substring(
                this._head,
                this._head + (this._buffer.length - this._head + 1)
            )
            .match(regexNumber);

        if (!match || !match[0]) {
            this.error('critical error scanning number');
            return { type: TokenType.Eof, line: -1, col: -1 };
        }

        this._head += match[0].length;

        return { type: TokenType.Integer, value: parseFloat(match[0]), line: lbegin, col: cbegin };
    }

    private next(): Token {
        if (this._lookAhead) {
            const t = { ...this._lookAhead };
            this.drop();
            return t;
        }

        this._lastToken = this.scanToken();

        return { ...this._lastToken };
    }

    private peek(): Token {
        if (!this._lookAhead) {
            this._lookAhead = this.scanToken();
        }

        return { ...this._lookAhead! };
    }

    private jump(): string | null {
        let multi = false;
        let single = false;
        const start = this._head;
        let level = 1;

        this._lastJumpBlock.line = this._line;

        if (this.head() === '}') {
            this._lastJumpBlock.content = undefined;
            return null;
        }

        for (; this._head != this._tail; this.advance()) {
            if (this.head() === '\r' && this.peekChar('\n')) {
                this._col++;
                this._head++;
            }

            if (this.head() === '\n') {
                this._line++;
                this._col = 0;
                if (single) {
                    single = false;
                    continue;
                }
            } else if (multi && this.head() === '*' && this.peekChar('/')) {
                multi = false;
                this.advance();
                continue;
            } else if (multi || single) {
                continue;
            } else if (this.head() === '/' && this.peekChar('*')) {
                multi = true;
                this.advance();
                continue;
            }

            if (this.head() === '{') {
                level++;
            } else if (this.head() === '}') {
                level--;
            }

            if (level === 0) {
                this._head--;
                this._line--;
                const content = this._buffer.substring(
                    start,
                    start + (this._head + 1 - start)
                );
                this._lastJumpBlock.content = content;
                return content;
            }
        }

        this._lastJumpBlock = { line: 0 };
        return null;
    }

    private getLastJumpBlock(): Block {
        return this._lastJumpBlock;
    }

    private tryExpect(type: TokenType, count: number) {
        for (let i = 0; i < count; i++) {
            if (this.expect(type, undefined, (i !== 0))) {
                return;
            }
        }
    }

    private skipToNextLine() {
        while (this._head !== this._tail) {
            this.advance();
            if (this.head() === '\n') {
                this.advance();
                break;
            }
        }
    }

    private error(msg: string, ignoreDiagnostic?: boolean): void {
        const range = Range.create(this._line, this._col, this._line, this._col + 1);
        if (this._rootSymbol) {
            this._rootSymbol.range.end = { line: this._line, character: this._col };
            this._rootSymbol.selectionRange.end = { line: this._line, character: this._col };

            if (!this._keySymbol) {
                range.start = this._rootSymbol.range.start;
                range.end = this._rootSymbol.range.end;
            }
        }
        if (this._keySymbol) {
            this._keySymbol.range.end = { line: this._line, character: this._col };
            this._keySymbol.selectionRange.end = { line: this._line, character: this._col };

            range.start = this._keySymbol.range.start;
            range.end = this._keySymbol.range.end;
        }

        if (!ignoreDiagnostic) {
            // this.diagnostics.push(Diagnostic.create(range, msg, DiagnosticSeverity.Error));
        }

        this._head = this._tail;
        // this.skipToNextLine();
    }
}
