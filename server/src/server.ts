import {
    createConnection,
    TextDocuments,
    ProposedFeatures,
    DidChangeConfigurationNotification,
    CompletionItem,
    TextDocumentPositionParams,
    TextDocumentSyncKind,
    InitializeResult,
    Hover,
    HoverParams,
    DocumentDiagnosticReportKind,
    MarkupKind,
} from 'vscode-languageserver/node';

import { TextDocument } from 'vscode-languageserver-textdocument';

import { Parser } from './parser';
import { Docs } from './docs';
import { rangeContains } from './util';

const connection = createConnection(ProposedFeatures.all);

const documents: TextDocuments<TextDocument> = new TextDocuments(TextDocument);

class Cache {
    private _cache: Map<string, Parser> = new Map();

    public get(uri: string) : Parser {
        let parser = this._cache.get(uri);
        if (parser) {
            return parser;
        }

        parser = new Parser(uri);
        this._cache.set(uri, parser);
        return parser;
    }

    public remove(uri: string) {
        this._cache.delete(uri);
    }
}

const cache = new Cache();

connection.onInitialize(() => {
    const result: InitializeResult = {
        capabilities: {
            textDocumentSync: TextDocumentSyncKind.Incremental,
            completionProvider: {
                resolveProvider: true,
                triggerCharacters: ['=', ',', ' '],
            },
            documentLinkProvider: {
                resolveProvider: false,
            },
            hoverProvider: true,
            documentSymbolProvider: true,
            diagnosticProvider: {
                workspaceDiagnostics: false,
                identifier: "languageServerOMWFX",
                interFileDependencies: false,
            }
        },
    };
    return result;
});

connection.onInitialized(() => {
    connection.client.register(
        DidChangeConfigurationNotification.type,
        undefined
    );
});

interface Settings {
    maxNumberOfProblems: number;
}

const documentSettings: Map<string, Thenable<Settings>> = new Map();

connection.onDidChangeConfiguration(() => {
    documentSettings.clear();
    documents.all().forEach(validateTextDocument);
});

function getDocumentSettings(resource: string): Thenable<Settings> {
    let result = documentSettings.get(resource);
    if (!result) {
        result = connection.workspace.getConfiguration({
            scopeUri: resource,
            section: 'languageServerOMWFX',
        });
        documentSettings.set(resource, result);
    }
    return result;
}

documents.onDidClose((e) => {
    documentSettings.delete(e.document.uri);
    cache.remove(e.document.uri);
});

documents.onDidChangeContent((change) => {
    validateTextDocument(change.document);
});

async function validateTextDocument(textDocument: TextDocument): Promise<void> {
    await getDocumentSettings(textDocument.uri);

    const text = textDocument.getText();

    try {
        const parser = cache.get(textDocument.uri);
        parser.parse(text);
    } catch (e) {
        if (e instanceof Error) {
            connection.console.error(
                `Failed parsing ${textDocument.uri}: ${e.message}`
            );
        } else {
            connection.console.error(`Failed parsing ${textDocument.uri}: e`);
        }
    }
}

let resultIdCounter = 1;
connection.languages.diagnostics.on(async (param) => {
    const uri = param.textDocument.uri;
    const parser = cache.get(uri);
    return { kind: DocumentDiagnosticReportKind.Full, items: parser.diagnostics, resultId: `${resultIdCounter++}` };
});

connection.onDidChangeWatchedFiles(() => {
});

connection.onCompletion(
    (documentPosition: TextDocumentPositionParams): CompletionItem[] => {
        const uri = documentPosition.textDocument.uri;
        const parser = cache.get(uri);
        return parser.getCompletion(documentPosition.position);
    }
);

connection.onCompletionResolve((item: CompletionItem): CompletionItem => {
    return item;
});

connection.onDocumentLinks((params) => {
    const parser = cache.get(params.textDocument.uri);
    return parser.getLinks();
})

connection.onDocumentSymbol((identifier) => {
    const parser = cache.get(identifier.textDocument.uri);
    return parser.symbols;
})

connection.onHover((textPosition: HoverParams): Hover | undefined  => {
    const parser = cache.get(textPosition.textDocument.uri);
    const symbol = parser.getLeafSymbolAtPosition(textPosition.position);

    if (!symbol) {
        return;
    }

    if (symbol.docRange && !rangeContains(symbol.docRange, textPosition.position)) {
        return;
    }
    
    const docString: string | undefined = Docs[symbol.token ? symbol.token.type : symbol.name];

    if (!docString) {
        return;
    }

    return {
        contents: {
            kind: MarkupKind.Markdown,
            value: docString,
        }
    }
});

documents.listen(connection);

connection.listen();
