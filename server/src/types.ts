import { Constants } from "./constants";

export type VoidFunction = () => void;

export type Block = {
    line: number;
    content?: string;
};

export enum TokenType {
    Float,
    Integer,
    Boolean,
    tring,
    Literal,
    String,
    Equal,
    Open_bracket,
    Close_bracket,
    Open_Parenthesis,
    Close_Parenthesis,
    Quote,
    SemiColon,
    Comma,
    VBar,
    Colon,
    Shared,
    Technique,
    Render_Target,
    Vertex,
    Fragment,
    Compute,
    Sampler_1D,
    Sampler_2D,
    Sampler_3D,
    Uniform_Bool,
    Uniform_Float,
    Uniform_Int,
    Uniform_Vec2,
    Uniform_Vec3,
    Uniform_Vec4,
    True,
    False,
    Vec2,
    Vec3,
    Vec4,
    Eof,
}

export type Token = {
    type: TokenType;
    value?: any;
    line: number;
    col: number;
};

export type Pass = {
    name?: string;
    target?: string;
    vertex?: string;
    fragment?: string;
    compute?: string;
    renderTargets?: any;
    blendSource?: Constants.BlendFunc;
    blendDest?: Constants.BlendFunc;
    blendEq?: Constants.BlendEquation;
    clearColor?: Array<number>[4]; // TODO: can make a color for syntax highlighting
    clear?: boolean;
    type?: TokenType;
};
